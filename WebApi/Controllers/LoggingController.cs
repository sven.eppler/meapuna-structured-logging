using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

public class LoggingController : ControllerBase {
  private readonly ILogger<LoggingController> logger;

  public LoggingController(ILogger<LoggingController> logger)
  {
    this.logger = logger;
  }

  [HttpGet("/loglevels")]
  public IActionResult LogLevels() {
    // Depending on the LogLevel-Configuration only some of these Log-Statements will be shown
    logger.LogTrace("Trace messages are used for in depth debug data. Like dumping entire data structures");
    logger.LogDebug("Debug messages are used for debugging help, to understand the program flow better");
    logger.LogInformation("Information messages are used to provide generall information what the program is doing");
    logger.LogWarning("Warning messages are used to highlight stuff that was not like expected but could be fixed on the go. Like missing params which fall back to default values, etc.");
    logger.LogError("Error messages are used to signal that something failed. This may be due wrong input values or other stuff beyond the scope of the program");
    logger.LogCritical("Critical messages are the last escalation step. They typically indicate unrecoverable errors which may also lead to a full shutdown of the program (like the database is not reachable, disk space empty, etc.).");

    return Ok();
  }

  [HttpGet("/logscopes")]
  public IActionResult LogScopes() {
    // Log scopes are used to include the same data in all nested log statements.


    var scopeData = new Dictionary<string, string> {
      { "GottesdienstId", "4c3030dd-ecca-4dbf-9854-908c57879a98" },
      { "AssignedRoles", "Pfarrer, Admin, Orgelspieler" }
    };

    logger.LogInformation("Before the LogScope");

    using(var logScope = logger.BeginScope(scopeData)) {
      logger.LogInformation("Started the logScope");

      // LogScopes may be nested
      using(var nestedLogScope = logger.BeginScope(new Dictionary<string, string> { { "nested", "we have to go deeper"}})) {
        logger.LogError("Uuups!");
      }
    }

    logger.LogInformation("After the LogScope");

    return Ok();
  }

  [HttpGet("/exceptions")]
  public IActionResult Exceptions() {
    try {
      throw new Exception("Hello, i am an exception!");
    }
    catch(Exception ex) {
      logger.LogError(ex, "We catched this...");
    }

    try {
      CallstackDummy1();
    }
    catch(Exception ex) {
      logger.LogError(ex, "Catched a deeper exception");
    }

    var innerInnerException = new ArgumentException("Cannot load stuff from database");
    var innerException = new NullReferenceException("Something cannot be null", innerInnerException);
    var exception = new Exception("Tried to do something, than this happened", innerException);
    logger.LogError(exception, "Nested exceptions");

    return Ok();
  }

  public void CallstackDummy1() {
    CallstackDummy2();
  }

  public void CallstackDummy2() {
    CallstackDummy3();
  }

  public void CallstackDummy3() {
    throw new Exception("With Callstack.");
  }

  [HttpGet("/respond-with-status/{statusCode:int}")]
  public IActionResult RespondWithStatus(int statusCode) {
    return StatusCode(statusCode);
  }

  [HttpGet("/slow-request")]
  public async Task<IActionResult> SlowRequest() {
    await Task.Delay(5000);

    logger.LogInformation($"Some data {Guid.NewGuid()}");

    return Ok();
  }
}