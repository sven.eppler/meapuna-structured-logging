# Logaggregation mit Seq

## Structured Logging

Grundidee: Logs wie Daten behandeln die durchsuchbar sind. Dazu muss man aber der gewohnten LogMessage eine gewisse Struktur geben.

Hier nutzt man jetzt ein einfaches Key-Value-Konzept um die Logausgabe mit zusätzlichen Informationen anzureichern.

Dazu muss man eine bestimmt Log-Syntax einhalten die sich an die Syntax von `String.Format` orientiert:

```
var itemCount = 1234;
var elapsedMilliseconds = 12;

logger.LogInformation("Processed {itemCount} items in {elapsedMilliseconds}ms", itemCount, elapsedMilliseconds);
```
  Dieses Beispiel erzeugt diese LogAusgabe (z.B. auf der Konsole oder einem File)
  
  ```
  Processed 1234 items in 12ms
  ```

  Aber zusätzlich werden die Werte in den geschweiften Klammern (`{itemCount}` und `{elapsedMilliseconds}`) als sogenannte `LogFields` an die LogMessage "angehängt" und können mithilfe spezieller Toosl (z.B. Seq) angesehen und auch ausgewertet werden.

  Wichtig: Bei der gleichzeitigen Verwendung von [string interpolation](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/tokens/interpolated) und structured logging muss man aufpassen, dass man die für das structured logging vorgesehenen Werte in doppelte geschweifte KLammern packt.

  Beispiel:

  ```
  var name = "Tim";
  // No additional LogField, since {name} will be interpolated with the value of the name variable
  logger.LogInformation($"My name is {name}");

  var age = 22;
  // Creates additional LogField for name, but not for age, since age is just interpolated and name is "escaped"
  loger.LogInformation($"My name is {{name}} and i am {age} years old", name);
  ```

### See Also
* https://ranjeet.dev/all-about-structured-logging-introduction/

## Seq Asp.net core integration

* Integrate Serilog with ASP.net Core: https://github.com/serilog/serilog-aspnetcore
* Add Seq as LogSink to Serilog: https://github.com/datalust/serilog-sinks-seq

Siehe [Program.cs](https://gitlab.com/sven.eppler/meapuna-structured-logging/-/blob/main/WebApi/Program.cs) Zeile 12-17.

## Beispiel

1) Beispielsystem starten: `docker-compose up`
2) Seq UI öffnen: http://localhost:8080/
    * User: `admin`
    * Pass: `PYMIN0*01TIPZtq7!^QG1@xYlKyZZ5H&`
3) Beispiel LogMessages erzeugen: `curl http://localhost:8081/loglevels`
4) Seq UI neu laden

## Logbeispiele

* Siehe [WebApi/Controllers/LoggingController.cs](https://gitlab.com/sven.eppler/meapuna-structured-logging/-/blob/main/WebApi/Controllers/LoggingController.cs)


### Braindump
* Structure logging?
  * LogFields
  * LogScopes
  * Log Exceptions
* Seq Overview
  * Filtering by LogField
  * Charting LogFields over Time
  * Dashboards